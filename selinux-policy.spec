%define distro redhat
%define polyinstatiate n
%define monolithic n
%define BUILD_DOC 1
%define BUILD_TARGETED 1
%define BUILD_MINIMUM 1
%define BUILD_MLS 1
%define POLICYVER 33
%define POLICYCOREUTILSVER 3.4
%define CHECKPOLICYVER 3.2

Summary: SELinux policy configuration
Name:    selinux-policy
Version: 40.7
Release: 8
License: GPLv2+
URL:     https://github.com/fedora-selinux/selinux-policy/

Source0: https://github.com/fedora-selinux/selinux-policy/archive/refs/tags/v40.7.tar.gz

# Tool helps during policy development, to expand system m4 macros to raw allow rules
# Git repo: https://github.com/fedora-selinux/macro-expander.git
Source1: macro-expander

# We obtain Source2~Source24 from https://src.fedoraproject.org/rpms/selinux-policy/tree/master
Source2: modules-targeted-base.conf
Source3: booleans-targeted.conf
Source4: Makefile.devel
Source5: setrans-targeted.conf
Source6: modules-mls-base.conf
Source7: booleans-mls.conf
Source8: setrans-mls.conf
Source9: securetty_types-targeted
Source10: securetty_types-mls
Source11: booleans-minimum.conf
Source12: setrans-minimum.conf
Source13: securetty_types-minimum
Source14: customizable_types
Source15: users-mls
Source16: users-targeted
Source17: users-minimum
Source18: file_contexts.subs_dist
Source19: modules-targeted-contrib.conf
Source20: modules-mls-contrib.conf
Source21: selinux-policy.conf
Source22: permissivedomains.cil
Source23: booleans.subs_dist
Source24: rpm.macros

# We obtain container.fc, container.if and container.te from https://github.com/containers/container-selinux.
# Then run the command:
# tar czvf container-selinux.tgz container.fc container.if container.te
Source35: container-selinux.tgz

Patch0:  Allow-local_login-to-be-access-to-var-run-files-and-.patch
Patch1:  fix-selinux-label-for-hostname-digest-list.patch
Patch2:  add-allow-for-ldconfig-to-map-libsudo_util-so.patch
Patch3:  allow-ipmievd-to-read-the-process-state-proc-pid-of-.patch
Patch4:  add_userman_access_run_dir.patch
Patch5:  add-firewalld-fc.patch
Patch6:  add-allow-systemd-timedated-to-unlink-etc-link.patch
Patch7:  add-avc-for-os-1.patch
Patch8:  allow-rpcbind-to-bind-all-port.patch
Patch9:  add-avc-for-systemd-journald.patch
Patch10: add-avc-for-systemd.patch
Patch11: backport-Add-support-for-secretmem-anon-inode.patch
Patch12: add-avc-for-haveged.patch
Patch13: Allow-init_t-nnp-domain-transition-to-rngd_t.patch

Patch9000: add-qemu_exec_t-for-stratovirt.patch
Patch9001: fix-context-of-usr-bin-rpmdb.patch
Patch9002: Add-permission-open-to-files_read_inherited_tmp_file.patch
Patch9003: allow-httpd-to-put-files-in-httpd-config-dir.patch
Patch9004: allow-map-postfix_master_t.patch
Patch9005: add-rule-for-hostnamed-to-rpmscript-dbus-chat.patch
Patch9006: allow-init_t-create-fifo-file-in-net_conf-dir.patch 
Patch9007: Revert-Don-t-allow-kernel_t-to-execute-bin_t-usr_t-binaries.patch
Patch9008: Policy-for-restoring-kernel_t.patch
Patch9009: Allow-init_t-nnp-domain-transition-to-abrtd_t.patch

BuildArch: noarch
BuildRequires:  python3 gawk checkpolicy >= %{CHECKPOLICYVER} m4 policycoreutils-devel >= %{POLICYCOREUTILSVER} bzip2 gcc procps-ng
Requires(pre):  policycoreutils >= %{POLICYCOREUTILSVER}
Requires(post): /bin/awk /usr/bin/sha512sum
Requires: rpm-plugin-selinux
Requires: selinux-policy-any = %{version}-%{release}
Provides: selinux-policy-base = %{version}-%{release}
Suggests: selinux-policy-targeted

%description 
SELinux Base package for SELinux Reference Policy - modular.

%define common_params DISTRO=%{distro} UBAC=n DIRECT_INITRC=n MONOLITHIC=%{monolithic} MLS_CATS=1024 MCS_CATS=1024

%define makeCmds() \
%make_build %common_params UNK_PERMS=%3 NAME=%1 TYPE=%2 bare \
%make_build %common_params UNK_PERMS=%3 NAME=%1 TYPE=%2 conf \
cp -f selinux_config/booleans-%1.conf ./policy/booleans.conf \
cp -f selinux_config/users-%1 ./policy/users \

%define makeModulesConf() \
cp -f selinux_config/modules-%1-%2.conf  ./policy/modules-base.conf \
cp -f selinux_config/modules-%1-%2.conf  ./policy/modules.conf \
if [ %3 == "contrib" ];then \
	cp selinux_config/modules-%1-%3.conf ./policy/modules-contrib.conf; \
	cat selinux_config/modules-%1-%3.conf >> ./policy/modules.conf; \
fi; \

%define installCmds() \
%make_build %common_params UNK_PERMS=%3 NAME=%1 TYPE=%2 base.pp \
%make_build %common_params UNK_PERMS=%3 NAME=%1 TYPE=%2 validate modules \
make %common_params UNK_PERMS=%3 NAME=%1 TYPE=%2 DESTDIR=%{buildroot} install \
make %common_params UNK_PERMS=%3 NAME=%1 TYPE=%2 DESTDIR=%{buildroot} install-appconfig \
make %common_params UNK_PERMS=%3 NAME=%1 TYPE=%2 DESTDIR=%{buildroot} SEMODULE="%{_sbindir}/semodule -p %{buildroot} -X 100 " load \
%{__mkdir} -p %{buildroot}%{_sysconfdir}/selinux/%1/logins \
touch %{buildroot}%{_sysconfdir}/selinux/%1/contexts/files/file_contexts.subs \
install -m0644 selinux_config/securetty_types-%1 %{buildroot}%{_sysconfdir}/selinux/%1/contexts/securetty_types \
install -m0644 selinux_config/file_contexts.subs_dist %{buildroot}%{_sysconfdir}/selinux/%1/contexts/files \
install -m0644 selinux_config/setrans-%1.conf %{buildroot}%{_sysconfdir}/selinux/%1/setrans.conf \
install -m0644 selinux_config/customizable_types %{buildroot}%{_sysconfdir}/selinux/%1/contexts/customizable_types \
touch %{buildroot}%{_sysconfdir}/selinux/%1/contexts/files/file_contexts.bin \
touch %{buildroot}%{_sysconfdir}/selinux/%1/contexts/files/file_contexts.local \
touch %{buildroot}%{_sysconfdir}/selinux/%1/contexts/files/file_contexts.local.bin \
cp %{SOURCE23} %{buildroot}%{_sysconfdir}/selinux/%1 \
rm -f %{buildroot}%{_datadir}/selinux/%1/*pp*  \
%{_bindir}/sha512sum %{buildroot}%{_sysconfdir}/selinux/%1/policy/policy.%{POLICYVER} | cut -d' ' -f 1 > %{buildroot}%{_sysconfdir}/selinux/%1/.policy.sha512; \
rm -rf %{buildroot}%{_sysconfdir}/selinux/%1/contexts/netfilter_contexts  \
rm -rf %{buildroot}%{_sysconfdir}/selinux/%1/modules/active/policy.kern \
rm -f %{buildroot}%{_sharedstatedir}/selinux/%1/active/*.linked \
%nil

%define fileList() \
%defattr(-,root,root) \
%dir %{_sysconfdir}/selinux/%1 \
%config(noreplace) %{_sysconfdir}/selinux/%1/setrans.conf \
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/seusers \
%dir %{_sysconfdir}/selinux/%1/logins \
%dir %{_sharedstatedir}/selinux/%1/active \
%verify(not md5 size mtime) %{_sharedstatedir}/selinux/%1/semanage.read.LOCK \
%verify(not md5 size mtime) %{_sharedstatedir}/selinux/%1/semanage.trans.LOCK \
%dir %attr(700,root,root) %dir %{_sharedstatedir}/selinux/%1/active/modules \
%verify(not md5 size mtime) %{_sharedstatedir}/selinux/%1/active/modules/100/base \
%dir %{_sysconfdir}/selinux/%1/policy/ \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/policy/policy.%{POLICYVER} \
%{_sysconfdir}/selinux/%1/.policy.sha512 \
%dir %{_sysconfdir}/selinux/%1/contexts \
%config %{_sysconfdir}/selinux/%1/contexts/customizable_types \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/securetty_types \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/dbus_contexts \
%config %{_sysconfdir}/selinux/%1/contexts/x_contexts \
%config %{_sysconfdir}/selinux/%1/contexts/default_contexts \
%config %{_sysconfdir}/selinux/%1/contexts/virtual_domain_context \
%config %{_sysconfdir}/selinux/%1/contexts/virtual_image_context \
%config %{_sysconfdir}/selinux/%1/contexts/lxc_contexts \
%config %{_sysconfdir}/selinux/%1/contexts/systemd_contexts \
%config %{_sysconfdir}/selinux/%1/contexts/sepgsql_contexts \
%config %{_sysconfdir}/selinux/%1/contexts/openssh_contexts \
%config %{_sysconfdir}/selinux/%1/contexts/snapperd_contexts \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/default_type \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/failsafe_context \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/initrc_context \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/removable_context \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/userhelper_context \
%dir %{_sysconfdir}/selinux/%1/contexts/files \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/contexts/files/file_contexts \
%ghost %{_sysconfdir}/selinux/%1/contexts/files/file_contexts.bin \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/contexts/files/file_contexts.homedirs \
%ghost %{_sysconfdir}/selinux/%1/contexts/files/file_contexts.homedirs.bin \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/files/file_contexts.local \
%ghost %{_sysconfdir}/selinux/%1/contexts/files/file_contexts.local.bin \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/files/file_contexts.subs \
%{_sysconfdir}/selinux/%1/contexts/files/file_contexts.subs_dist \
%{_sysconfdir}/selinux/%1/booleans.subs_dist \
%config %{_sysconfdir}/selinux/%1/contexts/files/media \
%dir %{_sysconfdir}/selinux/%1/contexts/users \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/users/root \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/users/guest_u \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/users/xguest_u \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/users/user_u \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/users/staff_u \
%{_datadir}/selinux/%1/base.lst \
%{_datadir}/selinux/%1/modules-base.lst \
%{_datadir}/selinux/%1/modules-contrib.lst \
%{_datadir}/selinux/%1/nonbasemodules.lst \
%{_sharedstatedir}/selinux/%1/active/commit_num \
%{_sharedstatedir}/selinux/%1/active/users_extra \
%{_sharedstatedir}/selinux/%1/active/homedir_template \
%{_sharedstatedir}/selinux/%1/active/seusers \
%{_sharedstatedir}/selinux/%1/active/file_contexts \
%{_sharedstatedir}/selinux/%1/active/policy.kern \
%ghost %{_sharedstatedir}/selinux/%1/active/policy.linked \
%ghost %{_sharedstatedir}/selinux/%1/active/seusers.linked \
%ghost %{_sharedstatedir}/selinux/%1/active/users_extra.linked \
%verify(not md5 size mtime) %{_sharedstatedir}/selinux/%1/active/file_contexts.homedirs \
%verify(not md5 size mtime) %{_sharedstatedir}/selinux/%1/active/modules_checksum \
%nil

%define relabel() \
if [ -s %{_sysconfdir}/selinux/config ]; then \
    . %{_sysconfdir}/selinux/config &> /dev/null || true; \
fi; \
FILE_CONTEXT=%{_sysconfdir}/selinux/%1/contexts/files/file_contexts; \
if %{_sbindir}/selinuxenabled && [ "${SELINUXTYPE}" = %1 -a -f ${FILE_CONTEXT}.pre ]; then \
     %{_sbindir}/fixfiles -C ${FILE_CONTEXT}.pre restore &> /dev/null > /dev/null; \
     rm -f ${FILE_CONTEXT}.pre; \
fi; \
%{_sbindir}/restorecon -R /var/lib/rpm /usr/bin /usr/lib /usr/lib64 /usr/sbin \
if %{_sbindir}/restorecon -e /run/media -R /root /var/log /var/run /etc/passwd* /etc/group* /etc/*shadow* 2> /dev/null;then \
    continue; \
fi;

%define preInstall() \
if [ $1 -ne 1 ] && [ -s %{_sysconfdir}/selinux/config ]; then \
     for MOD_NAME in ganesha ipa_custodia kdbus; do \
        if [ -d %{_sharedstatedir}/selinux/%1/active/modules/100/$MOD_NAME ]; then \
           %{_sbindir}/semodule -n -d $MOD_NAME; \
        fi; \
     done; \
     . %{_sysconfdir}/selinux/config; \
     FILE_CONTEXT=%{_sysconfdir}/selinux/%1/contexts/files/file_contexts; \
     if [ "${SELINUXTYPE}" = %1 -a -f ${FILE_CONTEXT} ]; then \
        [ -f ${FILE_CONTEXT}.pre ] || cp -f ${FILE_CONTEXT} ${FILE_CONTEXT}.pre; \
     fi; \
     touch %{_sysconfdir}/selinux/%1/.rebuild; \
     if [ -e %{_sysconfdir}/selinux/%1/.policy.sha512 ]; then \
        POLICY_FILE=`ls %{_sysconfdir}/selinux/%1/policy/policy.* | sort | head -1` \
        sha512=`sha512sum $POLICY_FILE | cut -d ' ' -f 1`; \
	checksha512=`cat %{_sysconfdir}/selinux/%1/.policy.sha512`; \
	if [ "$sha512" == "$checksha512" ] ; then \
		rm %{_sysconfdir}/selinux/%1/.rebuild; \
	fi; \
   fi; \
fi;

%define postInstall() \
if [ -s %{_sysconfdir}/selinux/config ]; then \
    . %{_sysconfdir}/selinux/config &> /dev/null || true; \
fi; \
if [ -e %{_sysconfdir}/selinux/%2/.rebuild ]; then \
   rm %{_sysconfdir}/selinux/%2/.rebuild; \
   %{_sbindir}/semodule -B -n -s %2; \
fi; \
[ "${SELINUXTYPE}" == "%2" ] && %{_sbindir}/selinuxenabled && load_policy; \
if [ %1 -eq 1 ]; then \
   %{_sbindir}/restorecon -R /root /var/log /run /etc/passwd* /etc/group* /etc/*shadow* 2> /dev/null; \
else \
%relabel %2 \
fi;

%define modulesList() \
awk '$1 !~ "/^#/" && $2 == "=" && $3 == "module" { printf "%%s ", $1 }' ./policy/modules-base.conf > %{buildroot}%{_datadir}/selinux/%1/modules-base.lst \
awk '$1 !~ "/^#/" && $2 == "=" && $3 == "base" { printf "%%s ", $1 }' ./policy/modules-base.conf > %{buildroot}%{_datadir}/selinux/%1/base.lst \
if [ -e ./policy/modules-contrib.conf ];then \
	awk '$1 !~ "/^#/" && $2 == "=" && $3 == "module" { printf "%%s ", $1 }' ./policy/modules-contrib.conf > %{buildroot}%{_datadir}/selinux/%1/modules-contrib.lst; \
fi;

%define nonBaseModulesList() \
contrib_modules=`cat %{buildroot}%{_datadir}/selinux/%1/modules-contrib.lst` \
base_modules=`cat %{buildroot}%{_datadir}/selinux/%1/modules-base.lst` \
for i in $contrib_modules $base_modules; do \
    if [ $i != "sandbox" ];then \
        echo "%verify(not md5 size mtime) %{_sharedstatedir}/selinux/%1/active/modules/100/$i" >> %{buildroot}%{_datadir}/selinux/%1/nonbasemodules.lst \
    fi; \
done;

%define checkConfigConsistency() \
if [ -f %{_sysconfdir}/selinux/.config_backup ]; then \
    . %{_sysconfdir}/selinux/.config_backup; \
else \
    BACKUP_SELINUXTYPE=targeted; \
fi; \
if [ -s %{_sysconfdir}/selinux/config ]; then \
    . %{_sysconfdir}/selinux/config; \
    if ls %{_sysconfdir}/selinux/$BACKUP_SELINUXTYPE/policy/policy.* &>/dev/null; then \
        if [ "$BACKUP_SELINUXTYPE" != "$SELINUXTYPE" ]; then \
            sed -i 's/^SELINUXTYPE=.*/SELINUXTYPE='"$BACKUP_SELINUXTYPE"'/g' %{_sysconfdir}/selinux/config; \
        fi; \
    elif [ "%1" = "targeted" ]; then \
        if [ "%1" != "$SELINUXTYPE" ]; then \
            sed -i 's/^SELINUXTYPE=.*/SELINUXTYPE=%1/g' %{_sysconfdir}/selinux/config; \
        fi; \
    elif ! ls  %{_sysconfdir}/selinux/$SELINUXTYPE/policy/policy.* &>/dev/null; then \
        if [ "%1" != "$SELINUXTYPE" ]; then \
            sed -i 's/^SELINUXTYPE=.*/SELINUXTYPE=%1/g' %{_sysconfdir}/selinux/config; \
        fi; \
    fi; \
fi;

%define backupConfigLua() \
local sysconfdir = rpm.expand("%{_sysconfdir}") \
local config_file = sysconfdir .. "/selinux/config" \
local config_backup = sysconfdir .. "/selinux/.config_backup" \
os.remove(config_backup) \
if posix.stat(config_file) then \
    local f = assert(io.open(config_file, "r"), "Failed to read " .. config_file) \
    local content = f:read("*all") \
    f:close() \
    local backup = content:gsub("SELINUX", "BACKUP_SELINUX") \
    local bf = assert(io.open(config_backup, "w"), "Failed to open " .. config_backup) \
    bf:write(backup) \
    bf:close() \
end

%build

%prep 
%setup -n %{name}-%{version} -q
tar -C policy/modules/contrib -xf %{SOURCE35}

%autopatch -p1

mkdir selinux_config
for i in %{SOURCE2} %{SOURCE3} %{SOURCE4} %{SOURCE5} %{SOURCE6} %{SOURCE7} %{SOURCE8} %{SOURCE9} %{SOURCE10} %{SOURCE11} %{SOURCE12} %{SOURCE13} %{SOURCE14} %{SOURCE15} %{SOURCE16} %{SOURCE17} %{SOURCE18} %{SOURCE19} %{SOURCE20}; do
cp $i selinux_config
done

%install
%{__rm} -fR %{buildroot}
mkdir -p %{buildroot}%{_sysconfdir}/selinux
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
touch %{buildroot}%{_sysconfdir}/selinux/config
touch %{buildroot}%{_sysconfdir}/sysconfig/selinux
mkdir -p %{buildroot}%{_usr}/lib/tmpfiles.d/
cp %{SOURCE21} %{buildroot}%{_usr}/lib/tmpfiles.d/
mkdir -p %{buildroot}%{_bindir}
install -m 755 %{SOURCE1} %{buildroot}%{_bindir}/

mkdir -p %{buildroot}%{_datadir}/selinux/{targeted,mls,minimum,modules}/
mkdir -p %{buildroot}%{_sharedstatedir}/selinux/{targeted,mls,minimum,modules}/
mkdir -p %{buildroot}%{_datadir}/selinux/packages

make clean
%if %{BUILD_TARGETED}
cp %{SOURCE22} %{buildroot}/
%makeCmds targeted mcs allow
%makeModulesConf targeted base contrib
%installCmds targeted mcs allow
%{_sbindir}/semodule -p %{buildroot} -X 100 -s targeted -i %{buildroot}/permissivedomains.cil
rm -rf %{buildroot}/permissivedomains.cil
rm -rf %{buildroot}%{_sharedstatedir}/selinux/targeted/active/modules/100/sandbox
%make_build %common_params UNK_PERMS=allow NAME=targeted TYPE=mcs sandbox.pp
mv sandbox.pp %{buildroot}%{_datadir}/selinux/packages/sandbox.pp
%modulesList targeted
%nonBaseModulesList targeted
%endif

%if %{BUILD_MINIMUM}
mkdir -p %{buildroot}%{_datadir}/selinux/minimum
%makeCmds minimum mcs allow
%makeModulesConf targeted base contrib
%installCmds minimum mcs allow
rm -f %{buildroot}%{_sysconfdir}/selinux/minimum/modules/active/modules/sandbox.pp
rm -rf %{buildroot}%{_sharedstatedir}/selinux/minimum/active/modules/100/sandbox
%modulesList minimum
%nonBaseModulesList minimum
%endif

%if %{BUILD_MLS}
%makeCmds mls mls deny
%makeModulesConf mls base contrib
%installCmds mls mls deny
%modulesList mls
%nonBaseModulesList mls
%endif

rm -rf %{buildroot}%{_sharedstatedir}/selinux/{minimum,targeted,mls}/previous

mkdir -p %{buildroot}%{_mandir}
cp -R  man/* %{buildroot}%{_mandir}
make %common_params UNK_PERMS=allow NAME=targeted TYPE=mcs DESTDIR=%{buildroot} PKGNAME=%{name} install-docs
make %common_params UNK_PERMS=allow NAME=targeted TYPE=mcs DESTDIR=%{buildroot} PKGNAME=%{name} install-headers
mkdir %{buildroot}%{_datadir}/selinux/devel/
mv %{buildroot}%{_datadir}/selinux/targeted/include %{buildroot}%{_datadir}/selinux/devel/include
install -m 644 selinux_config/Makefile.devel %{buildroot}%{_datadir}/selinux/devel/Makefile
install -m 644 doc/example.* %{buildroot}%{_datadir}/selinux/devel/
install -m 644 doc/policy.* %{buildroot}%{_datadir}/selinux/devel/

mkdir -p %{buildroot}%{_rpmconfigdir}/macros.d
install -m 644 %{SOURCE24} %{buildroot}%{_rpmconfigdir}/macros.d/macros.selinux-policy
sed -i 's/SELINUXPOLICYVERSION/%{version}-%{release}/' %{buildroot}%{_rpmconfigdir}/macros.d/macros.selinux-policy
sed -i 's@SELINUXSTOREPATH@%{_sharedstatedir}/selinux@' %{buildroot}%{_rpmconfigdir}/macros.d/macros.selinux-policy

rm -rf selinux_config
%post
if [ ! -s %{_sysconfdir}/selinux/config ]; then
echo "
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=enforcing
# SELINUXTYPE= can take one of these three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected. 
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted

" > %{_sysconfdir}/selinux/config

     ln -sf ../selinux/config %{_sysconfdir}/sysconfig/selinux
     %{_sbindir}/restorecon %{_sysconfdir}/selinux/config 2> /dev/null || :
else
     . %{_sysconfdir}/selinux/config
fi
exit 0

%postun
if [ $1 = 0 ]; then
     %{_sbindir}/setenforce 0 2> /dev/null
     if [ ! -s %{_sysconfdir}/selinux/config ]; then
          echo "SELINUX=disabled" > %{_sysconfdir}/selinux/config
     else
          sed -i 's/^SELINUX=.*/SELINUX=disabled/g' %{_sysconfdir}/selinux/config
     fi
fi
exit 0

%files
%{!?_licensedir:%global license %%doc}
%license COPYING
%dir %{_usr}/share/selinux
%dir %{_usr}/share/selinux/packages
%dir %{_sysconfdir}/selinux
%ghost %config(noreplace) %{_sysconfdir}/selinux/config
%ghost %{_sysconfdir}/sysconfig/selinux
%{_usr}/lib/tmpfiles.d/selinux-policy.conf
%{_rpmconfigdir}/macros.d/macros.selinux-policy

%package sandbox
Summary: SELinux policy sandbox
Requires(pre): selinux-policy-base = %{version}-%{release} selinux-policy-targeted = %{version}-%{release}

%description sandbox
SELinux sandbox policy used for the policycoreutils-sandbox package

%post sandbox
rm -f /etc/selinux/*/modules/active/modules/sandbox.pp.disabled 2>/dev/null
rm -f %{_sharedstatedir}/selinux/*/active/modules/disabled/sandbox 2>/dev/null
semodule -n -X 100 -i /usr/share/selinux/packages/sandbox.pp
if /usr/sbin/selinuxenabled ; then
    /usr/sbin/load_policy
fi;
exit 0

%preun sandbox
if [ $1 -eq 0 ] ; then
    semodule -n -d sandbox 2>/dev/null
    if /usr/sbin/selinuxenabled ; then
        /usr/sbin/load_policy
    fi;
fi;
exit 0

%files sandbox
%verify(not md5 size mtime) /usr/share/selinux/packages/sandbox.pp

%package devel
Summary: SELinux policy devel
Requires(pre): selinux-policy = %{version}-%{release}
Requires: selinux-policy = %{version}-%{release} m4 checkpolicy >= %{CHECKPOLICYVER} /usr/bin/make
Requires(post): policycoreutils-devel >= %{POLICYCOREUTILSVER}

%description devel
SELinux policy development and man page package

%post devel
selinuxenabled && /usr/bin/sepolgen-ifgen 2>/dev/null
exit 0

%files devel
%{_bindir}/macro-expander
%dir %{_usr}/share/selinux/devel
%dir %{_usr}/share/selinux/devel/include
%{_usr}/share/selinux/devel/include/*
%{_usr}/share/selinux/devel/Makefile
%{_usr}/share/selinux/devel/example.*
%{_usr}/share/selinux/devel/policy.*
%ghost %{_sharedstatedir}/sepolgen/interface_info

%package help
Summary: SELinux policy documentation
Requires: selinux-policy = %{version}-%{release}

Provides: selinux-policy-doc = %{version}-%{release}
Obsoletes: selinux-policy-doc < %{version}-%{release}

%description help
SELinux policy documentation package

%files help
%{_mandir}/ru/*/*
%doc %{_usr}/share/doc/%{name}

%if %{BUILD_TARGETED}
%package targeted
Summary:   SELinux targeted base policy
Requires(pre): policycoreutils >= %{POLICYCOREUTILSVER} coreutils selinux-policy = %{version}-%{release}
Requires:  selinux-policy = %{version}-%{release}

Provides: selinux-policy-any = %{version}-%{release}
Obsoletes: selinux-policy-targeted-sources < 2
Obsoletes: mod_fcgid-selinux <= %{version}-%{release}
Obsoletes: cachefilesd-selinux <= 0.10-1
Conflicts: audispd-plugins <= 1.7.7-1
Conflicts: seedit
Conflicts: 389-ds-base < 1.2.7, 389-admin < 1.1.12
Conflicts: container-selinux < 2:1.12.1-22

%description targeted
SELinux Reference policy targeted base module.

%pretrans targeted -p <lua>
%backupConfigLua

%pre targeted
%preInstall targeted

%post targeted
%checkConfigConsistency targeted
%postInstall $1 targeted
exit 0

%posttrans targeted
%checkConfigConsistency targeted

%postun targeted
if [ $1 = 0 ]; then
    if [ -s %{_sysconfdir}/selinux/config ]; then
        source %{_sysconfdir}/selinux/config &> /dev/null || true
    fi
    if [ "$SELINUXTYPE" = "targeted" ]; then
        %{_sbindir}/setenforce 0 2> /dev/null
        if [ ! -s %{_sysconfdir}/selinux/config ]; then
            echo "SELINUX=disabled" > %{_sysconfdir}/selinux/config
        else
            sed -i 's/^SELINUX=.*/SELINUX=disabled/g' %{_sysconfdir}/selinux/config
        fi
    fi
fi
exit 0

%triggerin -- pcre
%{_sbindir}/selinuxenabled && %{_sbindir}/semodule -nB
exit 0

%triggerpostun -- selinux-policy-targeted < 3.12.1-74
rm -f %{_sysconfdir}/selinux/*/modules/active/modules/sandbox.pp.disabled 2>/dev/null
exit 0

%triggerpostun targeted -- selinux-policy-targeted < 3.13.1-138
CR=$'\n'
INPUT=""
for i in `find %{_sysconfdir}/selinux/targeted/modules/active/modules/ -name \*disabled`; do
    module=`basename $i | sed 's/.pp.disabled//'`
    if [ -d %{_sharedstatedir}/selinux/targeted/active/modules/100/$module ]; then
        touch %{_sharedstatedir}/selinux/targeted/active/modules/disabled/$p
    fi
done
for i in `find %{_sysconfdir}/selinux/targeted/modules/active/modules/ -name \*.pp`; do
    INPUT="${INPUT}${CR}module -N -a $i"
done
for i in $(find %{_sysconfdir}/selinux/targeted/modules/active -name \*.local); do
    cp $i %{_sharedstatedir}/selinux/targeted/active
done
echo "$INPUT" | %{_sbindir}/semanage import -S targeted -N
if %{_sbindir}/selinuxenabled ; then
        %{_sbindir}/load_policy
fi
exit 0

%files targeted -f %{buildroot}%{_datadir}/selinux/targeted/nonbasemodules.lst
%config(noreplace) %{_sysconfdir}/selinux/targeted/contexts/users/unconfined_u
%config(noreplace) %{_sysconfdir}/selinux/targeted/contexts/users/sysadm_u 
%fileList targeted
%verify(not md5 size mtime) %{_sharedstatedir}/selinux/targeted/active/modules/100/permissivedomains
%endif

%if %{BUILD_MINIMUM}
%package minimum
Summary:  SELinux minimum base policy
Requires(pre):  coreutils selinux-policy = %{version}-%{release}
Requires(post): policycoreutils-python-utils >= %{POLICYCOREUTILSVER}
Requires: selinux-policy = %{version}-%{release}

Provides: selinux-policy-any = %{version}-%{release}
Conflicts: seedit
Conflicts: container-selinux <= 1.9.0-9

%description minimum
SELinux Reference policy minimum base module.

%pretrans minimum -p <lua>
%backupConfigLua

%pre minimum
%preInstall minimum
if [ $1 -ne 1 ]; then
    %{_sbindir}/semodule -s minimum --list-modules=full | awk '{ if ($4 != "disabled") print $2; }' > %{_datadir}/selinux/minimum/instmodules.lst
fi

%post minimum
%checkConfigConsistency minimum
contribpackages=`cat %{_datadir}/selinux/minimum/modules-contrib.lst`
basepackages=`cat %{_datadir}/selinux/minimum/modules-base.lst`
if [ ! -d %{_sharedstatedir}/selinux/minimum/active/modules/disabled ]; then
    mkdir %{_sharedstatedir}/selinux/minimum/active/modules/disabled
fi
if [ $1 -eq 1 ]; then
for p in $contribpackages; do
    touch %{_sharedstatedir}/selinux/minimum/active/modules/disabled/$p
done
for p in $basepackages apache dbus inetd kerberos mta nis; do
    rm -f %{_sharedstatedir}/selinux/minimum/active/modules/disabled/$p
done
%{_sbindir}/semanage import -S minimum -f - << __eof
login -m  -s unconfined_u -r s0-s0:c0.c1023 __default__
login -m  -s unconfined_u -r s0-s0:c0.c1023 root
__eof
%{_sbindir}/restorecon -R /root /var/log /var/run 2> /dev/null
%{_sbindir}/semodule -B -s minimum
else
instpackages=`cat %{_datadir}/selinux/minimum/instmodules.lst`
for p in $contribpackages; do
    touch %{_sharedstatedir}/selinux/minimum/active/modules/disabled/$p
done
for p in $instpackages apache dbus inetd kerberos mta nis; do
    rm -f %{_sharedstatedir}/selinux/minimum/active/modules/disabled/$p
done
%{_sbindir}/semodule -B -s minimum
%relabel minimum
fi
exit 0

%posttrans minimum
%checkConfigConsistency minimum

%postun minimum
if [ $1 = 0 ]; then
    if [ -s %{_sysconfdir}/selinux/config ]; then
        source %{_sysconfdir}/selinux/config &> /dev/null || true
    fi
    if [ "$SELINUXTYPE" = "minimum" ]; then
        %{_sbindir}/setenforce 0 2> /dev/null
        if [ ! -s %{_sysconfdir}/selinux/config ]; then
            echo "SELINUX=disabled" > %{_sysconfdir}/selinux/config
        else
            sed -i 's/^SELINUX=.*/SELINUX=disabled/g' %{_sysconfdir}/selinux/config
        fi
    fi
fi
exit 0

%triggerpostun minimum -- selinux-policy-minimum < 3.13.1-138
if [ `ls -A %{_sharedstatedir}/selinux/minimum/active/modules/disabled/` ]; then
    rm -f %{_sharedstatedir}/selinux/minimum/active/modules/disabled/*
fi
CR=$'\n'
INPUT=""
for i in `find %{_sysconfdir}/selinux/minimum/modules/active/modules/ -name \*disabled`; do
    module=`basename $i | sed 's/.pp.disabled//'`
    if [ -d %{_sharedstatedir}/selinux/minimum/active/modules/100/$module ]; then
        touch %{_sharedstatedir}/selinux/minimum/active/modules/disabled/$p
    fi
done
for i in `find %{_sysconfdir}/selinux/minimum/modules/active/modules/ -name \*.pp`; do
    INPUT="${INPUT}${CR}module -N -a $i"
done
echo "$INPUT" | %{_sbindir}/semanage import -S minimum -N
if %{_sbindir}/selinuxenabled ; then
    %{_sbindir}/load_policy
fi
exit 0

%files minimum -f %{buildroot}%{_datadir}/selinux/minimum/nonbasemodules.lst
%config(noreplace) %{_sysconfdir}/selinux/minimum/contexts/users/unconfined_u
%config(noreplace) %{_sysconfdir}/selinux/minimum/contexts/users/sysadm_u 
%fileList minimum
%endif

%if %{BUILD_MLS}
%package mls 
Summary: SELinux mls base policy
Requires: policycoreutils-newrole >= %{POLICYCOREUTILSVER} setransd selinux-policy = %{version}-%{release}
Requires(pre): policycoreutils >= %{POLICYCOREUTILSVER} coreutils selinux-policy = %{version}-%{release}

Provides: selinux-policy-any = %{version}-%{release}
Obsoletes: selinux-policy-mls-sources < 2
Conflicts: seedit
Conflicts: container-selinux <= 1.9.0-9

%description mls 
SELinux Reference policy mls base module.

%pretrans mls -p <lua>
%backupConfigLua

%pre mls
%preInstall mls

%post mls
%checkConfigConsistency mls
%postInstall $1 mls
exit 0

%posttrans mls
%checkConfigConsistency mls

%postun mls
if [ $1 = 0 ]; then
    if [ -s %{_sysconfdir}/selinux/config ]; then
        source %{_sysconfdir}/selinux/config &> /dev/null || true
    fi
    if [ "$SELINUXTYPE" = "mls" ]; then
        %{_sbindir}/setenforce 0 2> /dev/null
        if [ ! -s %{_sysconfdir}/selinux/config ]; then
            echo "SELINUX=disabled" > %{_sysconfdir}/selinux/config
        else
            sed -i 's/^SELINUX=.*/SELINUX=disabled/g' %{_sysconfdir}/selinux/config
        fi
    fi
fi
exit 0

%triggerpostun mls -- selinux-policy-mls < 3.13.1-138
CR=$'\n'
INPUT=""
for i in `find %{_sysconfdir}/selinux/mls/modules/active/modules/ -name \*disabled`; do
    module=`basename $i | sed 's/.pp.disabled//'`
    if [ -d %{_sharedstatedir}/selinux/mls/active/modules/100/$module ]; then
        touch %{_sharedstatedir}/selinux/mls/active/modules/disabled/$p
    fi
done
for i in `find %{_sysconfdir}/selinux/mls/modules/active/modules/ -name \*.pp`; do
    INPUT="${INPUT}${CR}module -N -a $i"
done
echo "$INPUT" | %{_sbindir}/semanage import -S mls -N
if %{_sbindir}/selinuxenabled ; then
        %{_sbindir}/load_policy
fi
exit 0

%files mls -f %{buildroot}%{_datadir}/selinux/mls/nonbasemodules.lst
%config(noreplace) %{_sysconfdir}/selinux/mls/contexts/users/unconfined_u
%fileList mls
%endif

%changelog
* Tue Mar 04 2025 Linux_zhang <zhangruifang@h-partners.com> - 40.7-8
- add more avc(eg:create link unlink) for haveged 

* Wed Feb 26 2025 yixiangzhike <yixiangzhike007@163.com> - 40.7-7
- Allow init_t nnp domain transition to rngd_t

* Tue Feb 25 2025 Linux_zhang <zhangruifang@h-partners.com> - 40.7-6
- add avc for haveged

* Mon Dec 09 2024 wangjiang <app@cameyan.com> - 40.7-5
- Recovering the SELinux Label

* Tue Nov 05 2024 Linux_zhang <zhangruifang@h-partners.com> - 40.7-4
- Add support for secretmem anon inode

* Tue Aug 13 2024 liyanan<liyanan61@h-partners.com> - 40.7-3
- Allow init_t nnp domain transition to abrtd_t

* Thu Apr 11 2024 jinlun<jinlun@huawei.com> - 40.7-2
- update modules-targeted-contrib.conf

* Thu Dec 28 2023 jinlun<jinlun@huawei.com> - 40.7-1
- update version to 40.7
  - Allow chronyd-restricted read chronyd key files
  - Allow systemd-sleep set attributes of efivarfs files
  - Make name_zone_t and named_var_run_t a part of the mountpoint attribute
  - Update cifs interfaces to include fs_search_auto_mountpoints()
  - Allow map xserver_tmpfs_t files when xserver_clients_write_xshm is on
  - Add map_read map_write to kernel_prog_run_bpf
  - Add policy for nvme-stas
  - Make new virt drivers permissive
  - Allow named and ndc use the io_uring api
  - Allow sssd send SIGKILL to passket_child running in ipa_otpd_t

* Fri Jul 21 2023 jinlun<jinlun@huawei.com> - 38.21-1
- update version to 38.21

* Wed May 31 2023 luhuaxin<luhuaxin1@huawei.com> - 38.6-5
- backport some upstream patches

* Wed Mar 29 2023 luhuaxin<luhuaxin1@huawei.com> - 38.6-4
- allow login_pgm setcap permission

* Mon Mar 20 2023 jinlun<jinlun@huawei.com> - 38.6-3
- Don't allow kernel_t to execute bin_t/usr_t binaries without a transition

* Mon Feb 6 2023 luhuaxin<luhuaxin1@huawei.com> - 38.6-2
- allow init_t create fifo file in net_conf dir

* Wed Feb 1 2023 zhangguangzhi<zhangguangzhi3@huawei.com> - 38.6-1
- update version to 38.6

* Thu Dec 29 2022 lixiao<lixiaoemail2017@163.com> - 35.5-17
- add rule for hostnamed to rpmscript dbus chat

* Sat Dec 24 2022 lixiao<lixiaoemail2017@163.com> - 35.5-16
- add the dependency between packages

* Sun Dec 3 2022 lujie <lujie54@huawei.com> - 35.5-15
- modify the patch name for the problem of vendor hard code

* Sun Nov 27 2022 lujie <lujie54@huawei.com> - 35.5-14
- backport upstream patches

* Mon Sep 19 2022 xinghe <xinghe2@h-partners.com> - 35.5-13
- allow map postfix_master_t

* Thu Sep 15 2022 lujie <lujie54@huawei.com> - 35.5-12
- backport upstream patches

* Tue Sep 13 2022 lujie <lujie54@huawei.com> - 35.5-11
- backport upstream patches

* Tue Sep 13 2022 lujie <lujie54@huawei.com> - 35.5-10
- backport upstream patches

* Tue Sep 13 2022 lujie <lujie54@huawei.com> - 35.5-9
- backport upstream patches

* Fri Sep 2 2022 lujie <lujie54@huawei.com> - 35.5-8
- backport upstream patches

* Thu Aug 18 2022 xuwenlong <xuwenlong16@huawei.com> - 35.5-7
- Allow chage domtrans to sssd

* Mon Jun 27 2022 lujie <lujie54@huawei.com> - 35.5-6
- Allow domain transition to sssd_t and role access to sssd 

* Sat Jun 25 2022 luhuaxin <luhuaxin1@huawei.com> - 35.5-5
- allow httpd to create files in /etc/httpd

* Thu Apr 28 2022 luhuaxin <luhuaxin1@huawei.com> - 35.5-4
- add open permission to files_read_inherited_tmp_file

* Mon Feb 28 2022 lujie42 <lujie42@huawei.com> - 35.5-3
- fix context of /usr/bin/rpmdb

* Mon Feb 21 2022 lujie42 <lujie42@huawei.com> - 35.5-2
- selinux-requires macro shouldn't depend on policycoreutils-python

* Tue Jan 11 2022 lujie42 <lujie42@huawei.com> - 35.5-1
- update selinux-policy-3.14.2 to selinux-policy-35.5-1

* Fri Oct 8 2021 lujie42 <lujie42@huawei.com> -3.14.2-77
- Fix CVE-2020-24612

* Wed Sep 22 2021 lujie42 <572084868@qq.com> -3.14.2-76
- Set httpd_can_network_connect bool true

* Fri Sep 3 2021 lujie42 <572084868@qq.com> -3.14.2-75
- Add allow rasdaemon cap_sys_admin

* Tue Aug 31 2021 lujie42 <572084868@qq.com> -3.14.2-74
- Allow systemd hostnamed read udev runtime data

* Fri Aug 20 2021 ExtinctFire <shenyining_00@126.com> -3.14.2-73
- Add avc for systemd selinux page

* Fri Aug 20 2021 mingyang <yangming73@huawei.com> -3.14.2-72
- Add qemu_exec_t for stratovirt

* Thu Jul 22 2021 lujie42 <572084868@qq.com> - 3.14.2-71
- Add weak dep of selinux-policy-targeted

* Thu Jun 17 2021 luhuaxin <1539327763@qq.com> - 3.14.2-70
- iptables.fc: Add missing legacy-restore and legacy-save entries

* Mon Jun 7 2021 luhuaxin <1539327763@qq.com> - 3.14.2-69
- fix context of ebtables

* Mon May 31 2021 luhuaxin <1539327763@qq.com> - 3.14.2-68
- backport some upstream patches
  backport-Allow-systemd-logind-dbus-chat-with-fwupd.patch
  backport-Allow-auditd-manage-kerberos-host-rcache-files.patch
  backport-Add-dev_lock_all_blk_files-interface.patch
  backport-Allow-systemd-machined-create-userdbd-runtime-sock-f.patch
  backport-Define-named-file-transition-for-sshd-on-tmp-krb5_0..patch
  backport-Allow-nsswitch_domain-to-connect-to-systemd-machined.patch
  backport-Allow-unconfined_t-to-node_bind-icmp_sockets-in-node.patch
  backport-Create-macro-corenet_icmp_bind_generic_node.patch
  backport-Allow-traceroute_t-and-ping_t-to-bind-generic-nodes.patch
  backport-Allow-passwd-to-get-attributes-in-proc_t.patch
  backport-Allow-login_pgm-attribute-to-get-attributes-in-proc_.patch
  backport-Allow-syslogd_t-domain-to-read-write-tmpfs-systemd-b.patch
  backport-Allow-all-users-to-connect-to-systemd-userdbd-with-a.patch
  backport-Add-new-devices-and-filesystem-interfaces.patch
  backport-Add-lvm_dbus_send_msg-lvm_rw_var_run-interfaces.patch
  backport-Allow-domain-write-to-an-automount-unnamed-pipe.patch
  backport-Allow-dyntransition-from-sshd_t-to-unconfined_t.patch
  backport-Allow-initrc_t-create-run-chronyd-dhcp-directory-wit.patch
  backport-Update-systemd_resolved_read_pid-to-also-read-symlin.patch
  backport-Allow-systemd-resolved-manage-its-private-runtime-sy.patch
  backport-Allow-systemd-logind-manage-init-s-pid-files.patch
  backport-Add-systemd_resolved_write_pid_sock_files-interface.patch
  backport-Allow-nsswitch-domain-write-to-systemd-resolved-PID-.patch
  backport-sysnetwork.if-avoid-directly-referencing-systemd_res.patch
  backport-Allow-stub-resolv.conf-to-be-a-symlink.patch
  backport-Allow-domain-stat-proc-filesystem.patch
  backport-Allow-domain-write-to-systemd-resolved-PID-socket-fi.patch
  backport-Allow-systemd-machined-manage-systemd-userdbd-runtim.patch
  backport-Allow-domain-stat-the-sys-filesystem.patch
  backport-Allow-login_userdomain-write-inaccessible-nodes.patch
  backport-Allow-local_login_t-get-attributes-of-tmpfs-filesyst.patch
  backport-Allow-dhcpc_t-domain-transition-to-chronyc_t.patch
  backport-Allow-nsswitch_domain-read-cgroup-files.patch
  backport-Allow-IPsec-and-certmonger-to-use-opencryptoki-servi.patch
  backport-Create-chronyd_pid_filetrans-interface.patch

* Sat May 29 2021 luhuaxin <1539327763@qq.com> - 3.14.2-67
- allow kdump_t net_admin capability

* Sat Mar 27 2021 luhuaxin <1539327763@qq.com> - 3.14.2-66
- allow rpcbind to bind all port

* Fri Mar 5 2021 luhuaxin <1539327763@qq.com> - 3.14.2-65
- selinux_requires macro shouldn't depend on policycoreutils-python
- add avc for allowing systemd services to check selinux status
- add avc for allowing dovecot to bind smtp port

* Sun Dec 13 2020 luhuaxin <1539327763@qq.com> - 3.14.2-64
- add avc for openEuler

* Tue Dec 8 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-63
- add allow systemd timedated to unlink etc link

* Fri Dec 4 2020 luhuaxin <1539327763@qq.com> - 3.14.2-62
- rebuild selinux-policy with policycoreutils-3.1-5

* Thu Sep 24 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-61
- add add-firewalld-fc.patch

* Tue Sep 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-60
- add allow-systemd-hostnamed-and-logind-read-policy.patch

* Thu Sep 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-59
- add allow-systemd_machined_t-delete-userdbd-runtime-sock.patch

* Thu Sep 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-58
- add allow-systemd-machined-create-userdbd-runtime-sock-file.patch

* Fri Aug 28 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-57
- add add_userman_access_run_dir.patch

* Mon Jul 27 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-56
- update selinux

* Mon Jul 20 2020 steven <steven_ygui@163.com> - 3.14.2-55
- add patch Allow-systemd_logind_t-to-read-fixed-dist-device-BZ-.patch

* Thu Jun 4 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-54
- add map to zerp device at dev_rw_zero interface;
  allow ipmievd to read the process state (/proc/pid) of init;
  allow systemd to mount unlabeled filesystemd;
  fix selinux label for hostname digest list;
  solve shutdown permission denied caused by dracut

* Sat May 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-53
- allow passwd to map and write sssd var lib

* Fri Mar 20 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-52
- use container-selinux.tgz of 2.73, the same version as package container-selinux

* Tue Mar 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-51
- fix upgrade error

* Thu Mar 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-50
- fix upgrade error

* Sat Feb 29 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-49
- enable selinux

* Wed Feb 26 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-48
- update avc for openEuler

* Sun Jan 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-47
- set selinux to permissive

* Thu Jan 16 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-46
- enable selinux; delete man

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-45
- update container-selinux.tgz

* Mon Dec 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-44
- add URL

* Fri Dec 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-43
- add source of tarball

* Mon Dec 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-42
- add allow for ldconfig to map /usr/libexec/libsudo_util.so
  allow syslogd_t domain to send null signal to all domain

* Thu Sep 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.14.2-41
- Package init
